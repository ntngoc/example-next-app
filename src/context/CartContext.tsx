import React, { createContext, ReactNode, useReducer } from 'react';
import { CartContextType, CartActionType, CartItem } from '../types/cart.type';
import { reducerCartContext } from './CartContext.reducer';

const initialState = {
  items: [],
  totalAmount: 0,
};

export const CartContext = createContext<CartContextType>({
  state: initialState,
  addItem: item => console.log(item),
  removeItem: index => console.log(index),
});

type Props = {
  children: ReactNode;
};

function CartContextProvider({ children }: Props) {
  const [state, dispatch] = useReducer(reducerCartContext, initialState);

  const addItem = (item: CartItem) => {
    dispatch({ type: CartActionType.ADD_ITEM, data: item });
  };

  const removeItem = (index: number) => {
    dispatch({ type: CartActionType.REMOVE_ITEM, data: index });
  };

  return <CartContext.Provider value={{ state, addItem, removeItem }}>{children}</CartContext.Provider>;
}

export default CartContextProvider;
