import { CartAction, CartActionType, CartItem, CartState } from '../types/cart.type';

function handleAddItem(state: CartState, newItem: CartItem): CartState {
  const index = state.items.findIndex(item => item.id == newItem.id);
  const _newItem = { ...newItem };
  if (index >= 0) {
    _newItem.quantity += state.items[index].quantity;
  }
  return {
    items:
      index >= 0
        ? [...state.items.slice(0, index), _newItem, ...state.items.slice(index + 1)]
        : [_newItem, ...state.items],
    totalAmount: state.totalAmount + newItem.price * newItem.quantity,
  };
}
function handleRemoveItem(state: CartState, index: number): CartState {
  return {
    items: [...state.items.slice(0, index), ...state.items.slice(index + 1)],
    totalAmount: state.totalAmount - state.items[index].price * state.items[index].quantity,
  };
}

export function reducerCartContext(state: CartState, action: CartAction): CartState {
  switch (action.type) {
    case CartActionType.ADD_ITEM:
      return handleAddItem(state, action.data as CartItem);
    case CartActionType.REMOVE_ITEM:
      return handleRemoveItem(state, action.data as number);
    default:
      throw new Error();
  }
}
