import { createContext, ReactNode, useState } from 'react';
import { clearToken, postToken } from '../helpers/jwt';
import { User } from '../types/user.type';

export type AuthContextType = {
  user?: User;
  login: (id: number, pwd: string) => Promise<boolean>;
  logout: () => Promise<boolean>;
};

export const AuthContext = createContext<AuthContextType>({
  user: undefined,
  login: async () => false,
  logout: async () => false,
});

type Props = {
  children: ReactNode;
};

export function AuthContextProvider({ children }: Props) {
  const [user, setUser] = useState<User>();

  const login = async (id: number, pwd: string) => {
    // TODO: handle login
    console.log({ id, pwd });
    setUser({ id: 0, role: 'admin' });
    const result = await postToken({
      accessToken: '',
      refreshToken: '',
    });
    return result.status == 201;
  };

  const logout = async () => {
    // TODO: handle logout
    const result = await clearToken();
    return result.status == 200;
  };

  return (
    <AuthContext.Provider
      value={{
        user,
        login,
        logout,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
}
