import axios from 'axios';
import { Token } from '../types/token.type';

export function getToken() {
  return axios.get<Token>('/api/jwt').then(res => res.data);
}

export function postToken(token: Token) {
  const { accessToken, refreshToken } = token;

  return axios.post('/api/jwt', null, {
    headers: {
      'access-token': accessToken,
      'refresh-token': refreshToken,
    },
  });
}

export function clearToken() {
  return axios.delete('/api/jwt');
}

export function refreshToken() {
  // TODO: handle refresh token
}
