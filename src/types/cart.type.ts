import { Product } from './product.type';

export type CartItem = Product & { quantity: number };

export enum CartActionType {
  ADD_ITEM = 'ADD_ITEM',
  REMOVE_ITEM = 'REMOVE_ITEM',
}

export type CartState = {
  items: CartItem[];
  totalAmount: number;
};

export type CartAction = {
  type: CartActionType;
  data?: CartItem | number;
};

export type CartContextType = {
  state: CartState;
  addItem: (item: CartItem) => void;
  removeItem: (index: number) => void;
};
