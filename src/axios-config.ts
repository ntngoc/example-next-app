import Axios from 'axios';
import { configure } from 'axios-hooks';
import LRU from 'lru-cache';
import { AppConfig } from './app-config';
import { getToken, refreshToken } from './helpers/jwt';

const axios = Axios.create({
  baseURL: AppConfig.apiBase,
});

const cache = new LRU({ max: 10 });

// request interceptor to add token to request headers
axios.interceptors.request.use(
  async config => {
    const token = await getToken();

    if (token?.accessToken) {
      config.headers = {
        authorization: `Bearer ${token?.accessToken}`,
      };
    }
    return config;
  },
  error => Promise.reject(error),
);

// response interceptor intercepting 401 responses, refreshing token and retrying the request
axios.interceptors.response.use(
  response => response,
  async error => {
    const config = error.config;

    if (error.response.status === 401 && !config._retry) {
      // we use this flag to avoid retrying indefinitely if
      // getting a refresh token fails for any reason
      config._retry = true;
      await refreshToken();

      return axios(config);
    }

    return Promise.reject(error);
  },
);

configure({ axios, cache });
