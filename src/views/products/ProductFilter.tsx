import {
  FormControl,
  IconButton,
  InputAdornment,
  MenuItem,
  Select,
  SelectChangeEvent,
  TextField,
  Toolbar,
  Typography,
} from '@mui/material';
import React, { useEffect, useState } from 'react';
import { Product, ProductGetParams } from '../../types/product.type';
import SearchIcon from '@mui/icons-material/Search';
import { useTranslation } from 'next-i18next';
import AddIcon from '@mui/icons-material/Add';
import ProductCreateDialog from './ProductCreateDialog';

type Props = {
  filter: ProductGetParams;
  onChange: (newFilter: ProductGetParams) => void;
  onCreatedProduct: (data: Product) => void;
};

function ProductFilter({ filter, onChange, onCreatedProduct }: Props) {
  const { t } = useTranslation();
  const [limit, setLimit] = useState('10');
  const [keyword, setKeyword] = useState('');
  const [openCreate, setOpenCreate] = useState(false);

  const toggleCreate = () => {
    setOpenCreate(prev => !prev);
  };

  const handleChangeLimit = (e: SelectChangeEvent) => {
    setLimit(e.target.value);
  };

  const handleKeywordChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setKeyword(e.target.value);
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      if (keyword != filter.keyword) {
        onChange({ ...filter, keyword });
      }
    }, 500);
    return () => clearTimeout(timer);
  }, [keyword, filter, onChange]);

  useEffect(() => {
    if (filter.limit != parseInt(limit)) {
      onChange({ ...filter, limit: parseInt(limit) });
    }
  }, [filter, limit, onChange]);

  return (
    <Toolbar disableGutters>
      <Typography sx={{ flexGrow: 1, display: 'flex', alignItems: 'center' }} variant='h5'>
        {t('All Products')}
        <IconButton sx={{ marginLeft: 2 }} color='primary' onClick={toggleCreate}>
          <AddIcon />
        </IconButton>
      </Typography>
      <ProductCreateDialog open={openCreate} onClose={toggleCreate} onCreated={onCreatedProduct} />
      <TextField
        sx={{ marginRight: 2 }}
        size='small'
        placeholder={t('Search term...')}
        value={keyword}
        onChange={handleKeywordChange}
        InputProps={{
          endAdornment: (
            <InputAdornment position='end'>
              <SearchIcon />
            </InputAdornment>
          ),
        }}
      />
      <FormControl size='small'>
        <Select
          value={limit}
          onChange={handleChangeLimit}
          startAdornment={<InputAdornment position='start'>{t('Page Size')}:</InputAdornment>}
        >
          <MenuItem value={10}>10</MenuItem>
          <MenuItem value={20}>20</MenuItem>
          <MenuItem value={30}>30</MenuItem>
        </Select>
      </FormControl>
    </Toolbar>
  );
}

export default ProductFilter;
