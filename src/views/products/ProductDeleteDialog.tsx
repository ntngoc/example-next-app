import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  LinearProgress,
} from '@mui/material';
import React from 'react';
import useProductDelete from '../../hooks/product/useProductDelete';
import { Product } from '../../types/product.type';

type Props = {
  data: Product;
  open: boolean;
  onClose: () => void;
  onDeleted: () => void;
};

function ProductDeleteDialog({ data, open, onClose, onDeleted }: Props) {
  const [{ loading, error }, doDelete] = useProductDelete(data.id);

  const handleDelete = () =>
    doDelete().then(res => {
      if (res.status == 200) {
        onClose();
        onDeleted();
      }
    });

  return (
    <Dialog open={open} onClose={onClose} fullWidth maxWidth='sm'>
      <DialogTitle>Delete Product</DialogTitle>
      {loading && <LinearProgress />}
      <DialogContent>
        <DialogContentText>
          Are you sure to delete <strong>{data.name}</strong>?
        </DialogContentText>
        {error && <DialogContentText color={'error'}>Error: {error.message}</DialogContentText>}
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} disabled={loading}>
          Close
        </Button>
        <Button color='error' disabled={loading} onClick={handleDelete}>
          Delete
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default ProductDeleteDialog;
