import { Alert, Box, Button, ButtonGroup, CircularProgress, Grid, Typography } from '@mui/material';
import { useTranslation } from 'next-i18next';
import { useRouter } from 'next/router';
import React, { useCallback, useEffect, useState } from 'react';
import useProductDetail from '../../hooks/product/useProductDetail';
import { Product } from '../../types/product.type';
import ProductDeleteDialog from './ProductDeleteDialog';
import ProductEditDialog from './ProductEditDialog';

type Props = {
  id: number;
};

function ProductDetail({ id }: Props) {
  const router = useRouter();
  const { t } = useTranslation();
  const [{ data: remoteData, loading, error }] = useProductDetail(id);
  const [openEdit, setOpenEdit] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const [data, setData] = useState<Product>();

  const toggleEdit = useCallback(() => {
    setOpenEdit(prev => !prev);
  }, []);

  const toggleDelete = useCallback(() => {
    setOpenDelete(prev => !prev);
  }, []);

  const handleUpdatedProduct = (newData: Product) => {
    setData(newData);
  };

  useEffect(() => {
    setData(remoteData);
  }, [remoteData]);

  if (loading) {
    return (
      <Box textAlign={'center'} p={20}>
        <CircularProgress />
      </Box>
    );
  }

  if (error) {
    return (
      <Box py={2}>
        <Alert severity='error'>{error.message}</Alert>
      </Box>
    );
  }

  if (!data) {
    return (
      <Box py={2}>
        <Alert severity='error'>Invalid request</Alert>
      </Box>
    );
  }

  return (
    <Grid container spacing={2}>
      <Grid item>
        <Box component={'img'} src={data?.thumbnail} alt='product-image' />
      </Grid>
      <Grid item xs>
        <Typography gutterBottom variant='h5'>
          {data?.name}
        </Typography>
        <Typography gutterBottom>
          <Typography component={'span'} sx={{ width: 60 }} display='inline-block'>
            {t('ID')}:
          </Typography>
          <Typography component={'strong'} sx={{ fontWeight: 'bold' }}>
            {id}
          </Typography>
        </Typography>
        <Typography>
          <Typography component={'span'} sx={{ width: 60 }} display='inline-block'>
            {t('Price')}:
          </Typography>
          <Typography component={'strong'} sx={{ fontWeight: 'bold' }}>
            {data?.price}
          </Typography>
        </Typography>
        <ButtonGroup>
          <Button sx={{ marginTop: 5 }} color='info' onClick={router.back}>
            {t('Go back')}
          </Button>
          <Button sx={{ marginTop: 5 }} color='warning' onClick={toggleEdit}>
            {t('Modify')}
          </Button>
          <Button sx={{ marginTop: 5 }} color='error' onClick={toggleDelete}>
            {t('Delete')}
          </Button>
        </ButtonGroup>
        <ProductEditDialog open={openEdit} onClose={toggleEdit} initialData={data} onUpdated={handleUpdatedProduct} />
        <ProductDeleteDialog open={openDelete} onClose={toggleDelete} onDeleted={router.back} data={data} />
      </Grid>
    </Grid>
  );
}

export default ProductDetail;
