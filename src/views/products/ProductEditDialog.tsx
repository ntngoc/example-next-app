import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  FormControl,
  FormHelperText,
  LinearProgress,
  TextField,
} from '@mui/material';
import React from 'react';
import { Product } from '../../types/product.type';
// form
import { Controller, useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import useProductUpdate from '../../hooks/product/useProductUpdate';

type Props = {
  initialData: Product;
  open: boolean;
  onClose: () => void;
  onUpdated: (data: Product) => void;
};

const schema = yup.object().shape({
  name: yup.string().min(5).max(40).required(),
  price: yup.number().min(1).max(1000).required(),
});

function ProductEditDialog({ open, onClose, initialData, onUpdated }: Props) {
  const [{ loading, error }, doUpdate] = useProductUpdate(initialData.id);
  const {
    reset,
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      name: initialData.name,
      price: initialData.price,
    },
    mode: 'onChange',
    resolver: yupResolver(schema),
  });

  const onSubmit = (data: Partial<Product>) => {
    doUpdate({ data }).then(res => {
      if (res.status == 200) {
        onClose();
        onUpdated({ ...res.data, ...data }); // in real-project, it should be onUpdated(res.data);
        reset({
          name: data.name,
          price: data.price,
        });
      }
    });
  };

  return (
    <Dialog open={open} onClose={onClose} fullWidth maxWidth='sm'>
      <DialogTitle>Edit Product</DialogTitle>
      {loading && <LinearProgress />}
      <DialogContent>
        <Box id='product-edit-form' component={'form'} onSubmit={handleSubmit(onSubmit)} mt={2}>
          <FormControl fullWidth sx={{ mb: 3 }}>
            <Controller
              name='name'
              control={control}
              rules={{ required: true }}
              render={({ field: { value, onChange } }) => (
                <TextField
                  value={value}
                  label='Name'
                  onChange={onChange}
                  placeholder='Enter a name'
                  error={Boolean(errors.name)}
                />
              )}
            />
            {errors.name && <FormHelperText sx={{ color: 'error.main' }}>{errors.name.message}</FormHelperText>}
          </FormControl>
          <FormControl fullWidth sx={{ mb: 3 }}>
            <Controller
              name='price'
              control={control}
              rules={{ required: true }}
              render={({ field: { value, onChange } }) => (
                <TextField
                  value={value}
                  label='Price'
                  onChange={onChange}
                  placeholder='Enter a price'
                  error={Boolean(errors.price)}
                  type='number'
                />
              )}
            />
            {errors.price && <FormHelperText sx={{ color: 'error.main' }}>{errors.price.message}</FormHelperText>}
          </FormControl>
        </Box>
        {error && <DialogContentText color={'error'}>Error: {error.message}</DialogContentText>}
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} disabled={loading}>
          Close
        </Button>
        <Button type='submit' form='product-edit-form' color='success' disabled={loading}>
          Save
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default ProductEditDialog;
