import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import MenuItem from '@mui/material/MenuItem';
import AppLogo from './AppLogo';
import Link from 'next/link';
import { useRouter } from 'next/router';
import UserMenu from './UserMenu';
import { useTranslation } from 'next-i18next';
import ToggleLanguageButton from './ToggleLanguageButton';
import FloatingCart from './FloatingCart';

type Props = {
  children: React.ReactElement;
};

const AppLayout = ({ children }: Props) => {
  const router = useRouter();
  const { t } = useTranslation();
  const [anchorElNav, setAnchorElNav] = React.useState<null | HTMLElement>(null);

  const navItems = [
    { name: t('Dashboard'), path: '/' },
    { name: t('Products'), path: '/products' },
    { name: t('Sales'), path: '/sales' },
  ];

  const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElNav(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleNavItemClick = (path: string) => () => {
    handleCloseNavMenu();
    router.push(path);
  };

  return (
    <>
      <AppBar position='fixed'>
        <Container maxWidth='xl'>
          <Toolbar disableGutters>
            <Box
              sx={{
                mr: 2,
                display: { xs: 'none', md: 'block' },
              }}
            >
              <AppLogo />
            </Box>

            <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
              <IconButton
                size='large'
                aria-label='account of current user'
                aria-controls='menu-appbar'
                aria-haspopup='true'
                onClick={handleOpenNavMenu}
                color='inherit'
              >
                <MenuIcon />
              </IconButton>
              <Menu
                id='menu-appbar'
                anchorEl={anchorElNav}
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'left',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'left',
                }}
                open={Boolean(anchorElNav)}
                onClose={handleCloseNavMenu}
                sx={{
                  display: { xs: 'block', md: 'none' },
                }}
              >
                {navItems.map(({ name, path }) => (
                  <MenuItem key={path} onClick={handleNavItemClick(path)}>
                    <Typography textAlign='center'>{name}</Typography>
                  </MenuItem>
                ))}
              </Menu>
            </Box>

            <Box sx={{ display: { xs: 'block', md: 'none' }, flexGrow: 1 }}>
              <AppLogo />
            </Box>

            <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
              {navItems.map(({ name, path }) => (
                <Link key={path} href={path} passHref>
                  <Button sx={{ my: 2, color: 'white', display: 'block' }}>{name}</Button>
                </Link>
              ))}
            </Box>
            <Box sx={{ flexGrow: 0, px: 3 }}>
              <ToggleLanguageButton />
            </Box>
            <Box sx={{ flexGrow: 0 }}>
              <UserMenu />
            </Box>
          </Toolbar>
        </Container>
      </AppBar>
      <Box component='main' sx={{ p: 3 }}>
        <Toolbar />
        {children}
      </Box>
      <FloatingCart />
    </>
  );
};
export default AppLayout;
