import { Box, Button } from '@mui/material';
import { useTranslation } from 'next-i18next';
import { useRouter } from 'next/router';
import React from 'react';

function ToggleLanguageButton() {
  const router = useRouter();
  const { i18n } = useTranslation();
  const handleToggleLang = () => {
    const { pathname, asPath, query } = router;
    router.push({ pathname, query }, asPath, { locale: i18n.language == 'en' ? 'ko' : 'en' });
  };

  return (
    <Button onClick={handleToggleLang} sx={{ color: 'white' }}>
      <Box
        sx={{ marginRight: 1, borderBottom: i18n.language == 'en' ? 'solid 2px' : 'none', padding: 0, lineHeight: 1.2 }}
      >
        EN
      </Box>
      |
      <Box
        sx={{ marginLeft: 1, borderBottom: i18n.language == 'ko' ? 'solid 2px' : 'none', padding: 0, lineHeight: 1.2 }}
      >
        KO
      </Box>
    </Button>
  );
}

export default ToggleLanguageButton;
