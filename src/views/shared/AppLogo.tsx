import React from 'react';
import AdbIcon from '@mui/icons-material/Adb';
import { Box, Typography } from '@mui/material';

function AppLogo() {
  return (
    <Box sx={{ display: 'flex', alignItems: 'center' }}>
      <AdbIcon sx={{ mr: 1 }} />
      <Typography
        variant='h6'
        noWrap
        component='a'
        href='/'
        sx={{
          fontFamily: 'monospace',
          fontWeight: 700,
          letterSpacing: '.3rem',
          color: 'inherit',
          textDecoration: 'none',
        }}
      >
        LOGO
      </Typography>
    </Box>
  );
}

export default AppLogo;
