import '../styles/globals.css';
import '../axios-config';
import type { AppProps } from 'next/app';
import { AuthContextProvider } from '../context/AuthContext';
import createEmotionCache from '../createEmotionCache';
import { CacheProvider, EmotionCache } from '@emotion/react';
import Head from 'next/head';
import { ThemeProvider } from '@mui/material/styles';
import { CssBaseline } from '@mui/material';
import theme from '../theme';
import { NextPage } from 'next';
import { appWithTranslation } from 'next-i18next';
import AppLayout from '../views/shared/AppLayout';
import CartContextProvider from '../context/CartContext';

// Client-side cache, shared for the whole session of the user in the browser.
const clientSideEmotionCache = createEmotionCache();

type ExtendedAppProps = AppProps & {
  Component: NextPage;
  emotionCache: EmotionCache;
};

function MyApp({ Component, emotionCache = clientSideEmotionCache, pageProps }: ExtendedAppProps) {
  return (
    <CacheProvider value={emotionCache}>
      <Head>
        <meta name='viewport' content='initial-scale=1, width=device-width' />
      </Head>
      <ThemeProvider theme={theme}>
        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
        <CssBaseline />
        <AuthContextProvider>
          <CartContextProvider>
            <AppLayout>
              <Component {...pageProps} />
            </AppLayout>
          </CartContextProvider>
        </AuthContextProvider>
      </ThemeProvider>
    </CacheProvider>
  );
}

export default appWithTranslation(MyApp);
