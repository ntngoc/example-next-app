import { Container } from '@mui/material';
import { useTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Head from 'next/head';
import { useRouter } from 'next/router';
import React from 'react';
import ProductDetail from '../../views/products/ProductDetail';

function ProductDetailPage() {
  const router = useRouter();
  const { id } = router.query;
  const { t } = useTranslation();

  return (
    <>
      <Head>
        <title>{t('Product Detail')}</title>
      </Head>
      <Container maxWidth='xl'>
        <ProductDetail id={parseInt(id as string)} />
      </Container>
    </>
  );
}

export const getServerSideProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
});

export default ProductDetailPage;
