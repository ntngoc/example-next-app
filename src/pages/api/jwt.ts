import { NextApiRequest, NextApiResponse } from 'next/types';

// eslint-disable-next-line
const cookie = require('cookie');

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method === 'GET') {
    const cookies = cookie.parse(req.headers.cookie || '');
    res.json({
      accessToken: cookies['access-token'],
      refreshToken: cookies['refresh-token'],
    });
  }
  if (req.method === 'POST') {
    const accessToken = req.headers['access-token'];
    const refreshToken = req.headers['refresh-token'];
    if (!accessToken) {
      res.statusCode = 401;
      res.statusMessage = 'Missing required header';

      return res.end();
    }
    res.setHeader('Set-Cookie', [
      cookie.serialize('access-token', String(accessToken), {
        httpOnly: true,
        maxAge: 60 * 60 * 24, // 1 day
      }),
      cookie.serialize('refresh-token', String(refreshToken), {
        httpOnly: true,
        maxAge: 60 * 60 * 24 * 7, // 7 day
      }),
    ]);
    res.statusCode = 201;

    return res.end();
  }
  if (req.method === 'DELETE') {
    res.setHeader('Set-Cookie', [
      cookie.serialize('access-token', String(''), {
        httpOnly: true,
        maxAge: 1,
      }),
      cookie.serialize('refresh-token', String(''), {
        httpOnly: true,
        maxAge: 1,
      }),
    ]);
    res.statusCode = 200;

    return res.end();
  }
  res.statusCode = 404;

  return res.end();
}
