import { Product } from '../../types/product.type';
import useAxios from '../shared/useAxiosWrapper';

/**
 * Topic: Manage Products
 *
 * Feature: Create a product
 *
 * @returns
 */
function useProductCreate(mockData?: Product) {
  return useAxios<Product>(
    {
      method: 'POST',
      url: '/products',
    },
    {
      manual: true,
      mockData,
    },
  );
}

export default useProductCreate;
