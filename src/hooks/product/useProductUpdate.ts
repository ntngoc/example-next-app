import { mockProductLists } from '../../mocks/product.mock';
import { Product } from '../../types/product.type';
import useAxios from '../shared/useAxiosWrapper';

/**
 * Topic: Manage Products
 *
 * Feature: Modify product data
 *
 * @returns
 */
function useProductUpdate(id: number) {
  return useAxios<Product>(
    {
      method: 'PUT',
      url: '/products/' + id,
    },
    {
      manual: true,
      mockData: mockProductLists.find(p => p.id == id),
    },
  );
}

export default useProductUpdate;
