import { mockProductLists } from '../../mocks/product.mock';
import { Product } from '../../types/product.type';
import useAxios from '../shared/useAxiosWrapper';

/**
 * Topic: Manage Products
 *
 * Feature: Delete a product
 *
 * @returns
 */
function useProductDelete(id: number) {
  return useAxios<Product>(
    {
      method: 'DELETE',
      url: '/products/' + id,
    },
    {
      manual: true,
      mockData: mockProductLists.find(p => p.id == id),
    },
  );
}

export default useProductDelete;
